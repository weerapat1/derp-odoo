from django.apps import AppConfig


class ApiodooConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apiodoo'
