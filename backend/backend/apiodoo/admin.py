from django.contrib import admin
from apiodoo.models import *
# Register your models here.

@admin.register(App)
class SellerAdmin(admin.ModelAdmin):
    list_display = [f.name for f in App._meta.fields]

@admin.register(FileType)
class SellerAdmin(admin.ModelAdmin):
    list_display = [f.name for f in FileType._meta.fields]

@admin.register(Logs)
class SellerAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Logs._meta.fields]
