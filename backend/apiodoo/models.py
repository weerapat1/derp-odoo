from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class App(models.Model):
    
    name = models.CharField(max_length=50)
    
    def __str__(self):
        return self.name


class FileType(models.Model):
    
    name = models.CharField(max_length=50)
    
    def __str__(self):
        return self.name

def user_directory_path(instance, filename):
    return 'user_{0}/{1}'.format(instance.user.id, filename)

class Logs(models.Model):
    
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    download_at = models.DateTimeField()
    file_type = models.ForeignKey(FileType, on_delete=models.CASCADE)
    from_app = models.ForeignKey(App, on_delete=models.CASCADE)
    file = models.FileField(upload_to=user_directory_path)
    
    def __str__(self):
        return self.user.username